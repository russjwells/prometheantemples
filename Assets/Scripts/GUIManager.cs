﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum MenuPage
{

    Main,
    Settings
}

public class GUIManager : MonoBehaviour {

    public const int NumMenuPages = 2;
    public bool menuIsVisible = false;
    public GameObject myMainMenu;
    public GameObject mySettingsMenu;
    public MenuPage currentMenuPage;
    public GameObject myHUD;
    public Text logDisplay;
    private string[] Log= new string[10];

	// Use this for initialization
	void Start () {
        var pos = new Vector3(0, 0, 0);
        Instantiate(myMainMenu, pos, Quaternion.identity);
        Instantiate(mySettingsMenu, pos, Quaternion.identity);
        Instantiate(myHUD, pos, Quaternion.identity);
        //somewhy I need the next 2 lines for things to work.
        myMainMenu = GameObject.FindGameObjectWithTag("MainMenu");
        mySettingsMenu = GameObject.FindGameObjectWithTag("SettingsMenu");
        myHUD = GameObject.FindGameObjectWithTag("HUD");
        myMainMenu.SetActive(false);
        mySettingsMenu.SetActive(false);

        currentMenuPage = (MenuPage)1;

        //Log[0]= "We have you.";
        
	}
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("StartButton")){
            toggleMenu();
        }
        if (menuIsVisible)
        {
            if (Input.GetButtonDown("LeftBumper"))
            {
                if (currentMenuPage == MenuPage.Main)
                {
                    mySettingsMenu.SetActive(true);
                    myMainMenu.SetActive(false);
                    currentMenuPage = MenuPage.Settings;
                }
                else
                {
                    myMainMenu.SetActive(true);
                    mySettingsMenu.SetActive(false);
                    currentMenuPage = MenuPage.Main;
                }
            }
            if (Input.GetButtonDown("RightBumper"))
            {
                if (currentMenuPage == MenuPage.Main)
                {
                    mySettingsMenu.SetActive(true);
                    myMainMenu.SetActive(false);
                    currentMenuPage = MenuPage.Settings;
                }
                else
                {
                    myMainMenu.SetActive(true);
                    mySettingsMenu.SetActive(false);
                    currentMenuPage = MenuPage.Main;
                }
            }
        }

        //logDisplay.text = Log[0];

	}

    public void logMsg(string message)
    {
        //update the log
        for (int i = 0; i < Log.Length; i++)
        {
            if (i < Log.Length-1)
            {
                Log[i + 1] = Log[i];
            }
            
        }
        Log[0] = message;

        //display the log
        string dispStr="";
        for (int i = 0; i < Log.Length; i++)
        {
            dispStr = dispStr+i.ToString() + "). " + Log[i];
        }
        logDisplay.text = dispStr;
    }

    public void toggleMenu()
    {
        if (menuIsVisible)
        {
            if (currentMenuPage == MenuPage.Main)
            {
                myMainMenu.SetActive(false);
                menuIsVisible = false;
            }
            if (currentMenuPage == MenuPage.Settings)
            {
                mySettingsMenu.SetActive(false);
                menuIsVisible = false;
            }
            
        }
        else
        {
            if (!menuIsVisible)
            {
                if (currentMenuPage == MenuPage.Main)
                {
                    myMainMenu.SetActive(true);
                    menuIsVisible = true;
                }
                if (currentMenuPage == MenuPage.Settings)
                {
                    mySettingsMenu.SetActive(true);
                    menuIsVisible = true;
                }
            }
        }
    }

    public void QuitButtonClick()
    {
        Application.Quit();
    }
}
