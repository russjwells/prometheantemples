﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Blink : MonoBehaviour {

    private bool blink = true;
    private int counter = 0;
    private int blinkSpeed = 10;
    public int onInterval = 200;
    public int offInterval = 50;
    public GameObject GUIObject;
    public Text GUIText;

	// Use this for initialization
	void Start () {
        GUIText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(counter == blinkSpeed){
            counter = 0;
            //this.guiTexture.enabled = false;
            if (blink)
            {
                GUIText.enabled = false;
                blink = false;
                blinkSpeed = offInterval;
            }
            else
            {
                GUIText.enabled = true;
                blink = true;
                blinkSpeed = onInterval;
            }
            
        }
        counter++;
//        Debug.Log(counter);
	}

    
 
}

