﻿using UnityEngine;


public class Player : Bolt.EntityBehaviour<IPlayerState> {

	private MazeCell currentCell;
	private MazeDirection currentDirection;
    private BoltEntity boltMaze;
    private Maze maze;
    public Camera myCam;
    public bool isArmed = false;
    public Transform RightHand;

    float LSHorizontal;
    float LSVertical;
    float RSHorizontal;
    float RSVertical;
    float LeftTrigger;
    float RightTrigger;

    bool AButton;
    bool BButton;
    bool XButton;
    bool YButton;
    bool StartButton;

    //player data
    public string Name;
    public int maxHealth;
    public int currentHealth;
    public int maxStamina;
    public int currentStamina;
    public int speed;
    public int dexterity;
    public int defence;
    public int luck;
    public int level;
    public int experience;

    PlayerMotor _motor;
    MouseLook _mouseLook;

	/*private void Rotate (MazeDirection direction) {
		transform.localRotation = direction.ToRotation();
		currentDirection = direction;
	}

	public void SetLocation (MazeCell cell) {
		if (currentCell != null) {
			currentCell.OnPlayerExited();
		}
		currentCell = cell;
	}

	private void Move (MazeDirection direction) {
		MazeCellEdge edge = currentCell.GetEdge(direction);
		if (edge is MazePassage) {
			SetLocation(edge.otherCell);
		}
	}*/

    public void setMaze(Maze myMaze)
    {
        maze = myMaze;
    }

    public override void Attached()
    {
        _motor = GetComponent<PlayerMotor>();
        _mouseLook = GetComponent<MouseLook>();

        state.Transform.SetTransforms(transform);

        DontDestroyOnLoad(this);
    }

    void PollKeys()
    {
        LSHorizontal = Input.GetAxisRaw("LSHorizontal");
        LSVertical = Input.GetAxisRaw("LSVertical");
        RSHorizontal = Input.GetAxisRaw("RSHorizontal");
        RSVertical = Input.GetAxisRaw("RSVertical");
        LeftTrigger = Input.GetAxisRaw("LeftTrigger");
        RightTrigger = Input.GetAxisRaw("RightTrigger");
        AButton = Input.GetButtonDown("AButton");
        BButton = Input.GetButtonDown("BButton");
        XButton = Input.GetButtonDown("XButton");
        YButton = Input.GetButtonDown("YButton");
        StartButton = Input.GetButtonDown("StartButton");
        //Debug.Log("Left: "+ LeftTrigger + " Right: "+RightTrigger);
    }

    void Update()
    {
        PollKeys();
        if (!isArmed)
        {
            if (LeftTrigger == 1)
            {
                Vector3 pos = RightHand.position;
                GameObject newZPCannon;
                //pos = new Vector3(0, 0, 0);
                newZPCannon=(GameObject)Instantiate(Resources.Load("ZPCannon"), pos, Quaternion.identity);
                newZPCannon.transform.parent = this.transform;
                isArmed = true;
            }
        }

    }

    public override void SimulateController()
    {
        PollKeys();

        IPlayerCommandInput input = PlayerCommand.Create();

        input.LSHorizontal = LSHorizontal;
        input.LSVeritcal = LSVertical;
        input.RSHorizontal = RSHorizontal;
        input.RSVertical = RSVertical;
        input.AButton = AButton;
        input.StartButton = StartButton;

        entity.QueueInput(input);
    }

    public override void ExecuteCommand(Bolt.Command command, bool resetState)
    {
        PlayerCommand cmd = (PlayerCommand)command;

        if (resetState)
        {
            _motor.SetState(cmd.Result.Position, cmd.Result.Velocity, cmd.Result.IsGrounded, cmd.Result.JumpFrames);
        }
        else
        {
            //move and save the resulting state //compute
            var result = _motor.Movement(cmd.Input.LSHorizontal, cmd.Input.LSVeritcal, cmd.Input.AButton);

            //look around
            _mouseLook.MouseRotation(cmd.Input.RSHorizontal, cmd.Input.RSVertical);

            //save movement
            cmd.Result.Position = result.position;
            cmd.Result.Velocity = result.velocity;
            cmd.Result.JumpFrames = result.jumpFrames;
            cmd.Result.IsGrounded = result.isGrounded;
        }
    }

}
