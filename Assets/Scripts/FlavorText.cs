﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class FlavorText : MonoBehaviour {

    public Text flavortext;
    public int delay;
    string[] FlavorTexts = new string[] 
    {
        "Population, climate, hunger... our leading sages and scientists say the cataclysm is coming.",
        "The Cataclysm is coming. You must journey into the temples and retrieve the Fire so that we can overcome.",
        "The Fire doesn't burn if you can hold its energy.",
        "There is a myth about the people who built the Temples. They were very strong, and very powerful yes, but in their minds... they were different. They could think as one. And the Fire didn't burn them.",
        "We must avert the Cataclysm!",
        "There are too many people out there!",
        "They say that the Temple Builders could enter Group Consciousness at will.",
        "How will the Fire save us? Isn't it just like regular fire?",
        "Grandpa says the Temple Builders knew how to grow forests that never ran out of food!",
        "There's not much food in the market a few villages over...",
        "The Warrior's path is not for All",
        "Fear is the mind killer. The little death that brings total obliteration.",
        "Reality's got nothing to do with it.",
        "real is a dirty, four-letter word",
        "You cannot look out into space without looking back into time.",
        "Who is the Master that makes the grass green?"
    };

    
    // Use this for initialization
	void Start () {
        int ran = (int)Mathf.Round(Random.Range(0,FlavorTexts.Length)); 
        flavortext.text= FlavorTexts[ran];
        StartCoroutine(FlavorHide());
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator FlavorHide()
    {
        yield return new WaitForSeconds(delay);
        GameObject.FindGameObjectWithTag("SceneFlavorCanvas").SetActive(false);
        //this.gameObject.SetActive(false);
    }
}
