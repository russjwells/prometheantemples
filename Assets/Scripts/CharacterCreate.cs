﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

enum Stat : int
{
    Health, Speed, Dexterity, Stamina, Luck, Defence
}

public class CharacterCreate : MonoBehaviour {

    public string Name;
    //private int ClassType;
    public int Health=0;
    public int Speed=0;
    public int Dexterity=0;
    public int Stamina=0;
    public int Luck=0;
    public int Defence=0;
    private bool hasName = false;

    public Text nameText;
    public InputField myInputField;
    public Text NameDisplay;
    public Text HealthDisplay;
    public Text SpeedDisplay;
    public Text DexDisplay;
    public Text StaDisplay;
    public Text LuckDisplay;
    public Text DefDisplay;
    public GameObject CharacterWindow;
    public GameObject NameWindow;
    public GameObject JoinIpWindow;
    public GameObject TitleWindow;
    bool titleClosed = false;

    public InputField ipInput;
    public InputField portInput;
    public string ipToConnect;
    public string portToConnect;
    bool StartButton;

    public InputField NameField;
    public GameObject StartGameButton;
    public GameObject JoinGameButton;


    string[] DefaultNames = new string[] 
    {
        "Hope",
        "Charity",
        "Joy",
        "Karn",
        "Bebub",
        "Ramsey",
        "Donert",
        "Skal",
        "Nup",
        "Kai"
    };

    
    // Use this for initialization
	void Start () {
        myInputField.Select();
        int ran = (int)Mathf.Round(Random.Range(0, DefaultNames.Length));
        NameField.text = DefaultNames[ran];
	}
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
	
	// Update is called once per frame
	void Update () {
        StartButton = Input.GetButtonDown("StartButton");
        if (titleClosed)
        {
            if (!hasName)
            {
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("AButton"))
                {
                    //CLOSE NAME WINDOW
                    setName(nameText.text);
                    hasName = true;
                    CharacterWindow.SetActive(true);
                    NameDisplay.text = Name;
                    Reroll();
                    NameWindow.SetActive(false);
                }
            }
        }
        else
        {
            if (StartButton)
            {
                NameWindow.SetActive(true);
                //NameField.ActivateInputField();
                TitleWindow.SetActive(false);
                titleClosed = true;
            }
            
        }
	}

    public void CreateCharacter()
    {
        //launch temple

    }
    public void setName(string n)
    {
        Name = n;
    }
    public void Reroll()
    {
        Health=0;
        Speed = 0;
        Dexterity = 0;
        Stamina = 0;
        Luck = 0;
        Defence = 0;
        int statToBoost;
        for (int x = 0; x < 20; x++)
        {
            statToBoost = (int)Mathf.Round(Random.Range(0, 6));
            //Debug.Log(statToBoost);
            if (statToBoost == 0)
            {
                Health++;
            }
            if (statToBoost == 1)
            {
                Speed++;
            }
            if (statToBoost == 2)
            {
                Dexterity++;
            }
            if (statToBoost == 3)
            {
                Stamina++;
            }
            if (statToBoost == 4)
            {
                Luck++;
            }
            if (statToBoost == 5)
            {
                Defence++;
            }
        }
        /*
        Health = (int)Mathf.Ceil(Random.Range(1, 5));
        Speed = (int)Mathf.Ceil(Random.Range(1, 5));
        Dexterity = (int)Mathf.Ceil(Random.Range(1, 5));
        Stamina = (int)Mathf.Ceil(Random.Range(1, 5));
        Luck = (int)Mathf.Ceil(Random.Range(1, 5));
        Defence = (int)Mathf.Ceil(Random.Range(1, 5));
        */

        HealthDisplay.text = Health.ToString();
        SpeedDisplay.text = Speed.ToString();
        DexDisplay.text = Dexterity.ToString();
        StaDisplay.text = Stamina.ToString();
        LuckDisplay.text = Luck.ToString();
        DefDisplay.text = Defence.ToString();
    }
    public void joinGame()
    {
        JoinIpWindow.SetActive(true);
    }
    public void SearchButton()
    {
        ipToConnect = "127.0.0.1";
        portToConnect = "27000";
        
        if (ipInput.text != null)
            ipToConnect = ipInput.text;
        if (portInput.text != null)
            portToConnect = portInput.text;

        // START CLIENT
        Debug.Log("Trying to connect with" + ipToConnect + ":" + portToConnect);
        BoltLauncher.StartClient();
        BoltNetwork.Connect(UdpKit.UdpEndPoint.Parse(ipToConnect + ":" + portToConnect));
        //this.SetActive(false);
    }

    public void hostGame()
    {
        BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse("0.0.0.0:27000"));
        StartGameButton.SetActive(true);
        JoinGameButton.SetActive(false);
    }
    public void startGame()
    {
        BoltNetwork.LoadScene("maze");
    }
}
