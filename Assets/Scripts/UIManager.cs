﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public InputField IPspot;
    public string IPAddy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	//debug.loh
	}

    public void newGame()
    {
        BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse("0.0.0.0:27000"));
        BoltNetwork.LoadScene("maze");
    }

    public void joinGame(string ip)
    {
        IPAddy = "127.0.0.1";
        //IPAddy = IPspot.text.text;
        //Debug.Log(IPspot.text);
        if (IPspot.text!=null)
        IPAddy = IPspot.text;
        // START CLIENT
        Debug.Log("Trying to connect with"+IPAddy);
        BoltLauncher.StartClient();
        BoltNetwork.Connect(UdpKit.UdpEndPoint.Parse(IPAddy+":27000"));
    }
}
