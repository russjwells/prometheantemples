﻿using UnityEngine;
using System.Collections;


public class GameManager : MonoBehaviour {

    enum World { Temple, Village };

    public Maze mazePrefab;
	private Maze mazeInstance;
	public Player playerPrefab;
	private Player playerInstance;
    private BoltEntity player;
    //private BoltEntity boltMaze;
    public ArrayList playerList = new ArrayList();
    
    public GameObject mySpawnPoint;
    private GameObject myExitPoint;
    private bool gameHasBegun=false;
    //public World currentWorld;

    
   
    //make playerInstance into an array

    void Start()
    {
        //StartCoroutine(BeginGame());
        if (gameHasBegun==false)
        {
            BeginGameInstantly();
            Debug.Log("Game Start!");
        }

        
    }
    void Awake()
    {
        DontDestroyOnLoad(this);
    }

	private IEnumerator BeginGame() {
            //currentWorld = Temple;
            //set camera to show maze creation
			Camera.main.clearFlags = CameraClearFlags.Skybox;
			Camera.main.rect = new Rect(0f, 0f, 1f, 1f);
            

            //start maze creation
            Instantiate(mazePrefab);
            mazeInstance = GameObject.FindGameObjectWithTag("Maze").GetComponent<Maze>();
            yield return StartCoroutine(mazeInstance.Generate());
            

            //show mini map
            Camera.main.clearFlags = CameraClearFlags.Depth;
            Camera.main.rect = new Rect(0f, 0f, 0.5f, 0.5f);

            //spawn players
            spawnServerPlayer();
            spawnWaitingClients();
            gameHasBegun = true;
	}

    private void BeginGameInstantly()
    {
//        Camera.main.clearFlags = CameraClearFlags.Skybox;
//        Camera.main.rect = new Rect(0f, 0f, 1f, 1f);
//        GameObject.FindGameObjectWithTag("MainCamera").SetActive(false);


        //start maze creation
        spawnNewTemple();

        
        //spawn players
        spawnServerPlayer();
        spawnWaitingClients();
        gameHasBegun = true;
    }

	private void RestartGame() {
			StopAllCoroutines();
			Destroy(mazeInstance.gameObject);
			if (playerInstance != null) {
				Destroy(playerInstance.gameObject);
			}
			StartCoroutine(BeginGame());
	}

    public void newPlayer(BoltConnection connection){

        bool mazeIsHere = true;

        //WaitForSeconds 1

        //wait for maze
        if (mazeIsHere)
        {
            // spawn player
            BoltEntity _character;
            _character = BoltNetwork.Instantiate(BoltPrefabs.NewPlayer);
            playerList.Add(_character);
            _character.transform.position = mySpawnPoint.transform.position;
            _character.AssignControl(connection);
        }
        
    }

    public void spawnNewTemple()
    {
        Instantiate(mazePrefab);
        mazeInstance = GameObject.FindGameObjectWithTag("Maze").GetComponent<Maze>();
        mazeInstance.GenerateInstantly();
        placeSpawnPoint();
        placeExitPoint();

        //show mini map
//        Camera.main.clearFlags = CameraClearFlags.Depth;
//        Camera.main.rect = new Rect(0f, 0f, 0.5f, 0.5f);
    }

    private void spawnServerPlayer()
    {
        //create server player
        var pos = mazeInstance.GetCell(mazeInstance.RandomCoordinates);
        player = BoltNetwork.Instantiate(BoltPrefabs.NewPlayer);
        playerList.Add(player);
        //playerList[0]
        player.TakeControl();
        //var plus = new Vector3(0, 10, 0);
        player.transform.position = mySpawnPoint.transform.position;
    }
    private void spawnWaitingClients()
    {

    }
    public void movePlayersToSpawn()
    {
        mySpawnPoint = GameObject.FindGameObjectWithTag("SpawnPoint");
        GameObject[] players;
        players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject p in players)
        {
            p.transform.position = mySpawnPoint.transform.position;
            Debug.Log("Player " + p + " moved to spawn.");
        }
        
    }
    private void placeSpawnPoint()
    {
        var ranPos = mazeInstance.GetCell(mazeInstance.RandomCoordinates).transform.position;
        ranPos.y = 0.5f;
        Instantiate(Resources.Load("SpawnPoint"), ranPos, Quaternion.identity);
        mySpawnPoint=GameObject.FindGameObjectWithTag("SpawnPoint");

    }
    private void placeExitPoint()
    {
        var ranPos = mazeInstance.GetCell(mazeInstance.RandomCoordinates).transform.position;
        ranPos.y = 0.5f;
        //Instantiate(Resources.Load("ExitPoint"), ranPos, Quaternion.identity);
        BoltNetwork.Instantiate(BoltPrefabs.ExitPoint, ranPos, Quaternion.identity);
        myExitPoint = GameObject.FindGameObjectWithTag("ExitPoint");
    }
}

// code name: maze escape, founded 1/28/2015 -russ
// MVP complete 2/22/2015 10:20am -russ, stoked.