﻿using UnityEngine;
using System.Collections;
//client code
[BoltGlobalBehaviour(BoltNetworkModes.Client)]
public class ClientCallbacks : Bolt.GlobalEventListener{

    GameObject myMaze;
    private Maze mazeInstance;

    public override void OnEvent(NewGameEvent evnt)
    {
        
        Random.seed=evnt.seed;
        Debug.Log("Recieved seed: " + evnt.seed);

        
    }
    public override void SceneLoadLocalDone(string map)
    {

        var pos = new Vector3(0, 0, 0);
        Instantiate(Resources.Load("Maze"), pos, Quaternion.identity);
        mazeInstance = GameObject.FindGameObjectWithTag("Maze").GetComponent<Maze>();
        mazeInstance.GenerateInstantly();
        Debug.Log("Client Instantiated Maze" + mazeInstance);



    }

    public override void ControlOfEntityGained(BoltEntity ply)
    {
        if (ply.gameObject.GetComponent<PlayerMotor>() != null)
        {
            ply.name = "Client Player";
            Debug.Log("yay I has player: "+ply.name);
            var plus = new Vector3(0, 50, 0);
            ply.transform.position = plus;
            ply.transform.Find("MyCamera").gameObject.SetActive(true);

            //show mini map
            Camera.main.clearFlags = CameraClearFlags.Depth;
            Camera.main.rect = new Rect(0f, 0f, 0.5f, 0.5f);
           
        }
    }

}
