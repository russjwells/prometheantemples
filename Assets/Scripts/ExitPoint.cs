﻿using UnityEngine;
using System.Collections;

//add this to your "level goal" trigger
[RequireComponent(typeof(Collider))]
public class ExitPoint : MonoBehaviour
{

    public float scoreDelay;			//how long player must stay inside the goal, before the game moves onto the next level
    private float counter;
    public string map;


    void Awake()
    {
        GetComponent<Collider>().isTrigger = true;
    }


    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("You found the end");
            BoltNetwork.LoadScene(map);
        }
        else
        {
            //Debug.Log("You're at the wrong base!");
        }
    }

    //if player leaves trigger, reset "how long they need to stay inside trigger for level to advance" timer
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            counter = 0f;
    }
}

