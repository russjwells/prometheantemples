﻿using UnityEngine;
using System.Collections;

public class MazeCell : MonoBehaviour {

	public MazeRoom room;
	public IntVector2 coordinates;
	private int initializedEdgeCount;

	public void Initialize (MazeRoom room) {
		room.Add(this);
		transform.GetChild(0).GetComponent<Renderer>().material = room.settings.floorMaterial;
	}

	public bool IsFullyInitialized {
		get {
			return initializedEdgeCount == MazeDirections.Count;
		}
	}

	public MazeDirection RandomUninitializedDirection {
		get {
			int skips = Random.Range(0, MazeDirections.Count - initializedEdgeCount);
			for (int i = 0; i < MazeDirections.Count; i++) {
				if (edges[i] == null) {
					if (skips == 0) {
						return (MazeDirection)i;
					}
					skips -=1;
				}
			}
			throw new System.InvalidOperationException("MazeCell has no uninitialized directions left.");
		}
	}

	public void SetEdge (MazeDirection direction, MazeCellEdge edge) {
		edges[(int)direction] = edge;
		initializedEdgeCount +=1;
	}

	private MazeCellEdge[] edges = new MazeCellEdge[MazeDirections.Count];

	public MazeCellEdge GetEdge (MazeDirection direction) {
		return edges[(int)direction];
	}

	public void OnPlayerEntered(){
		//room.Show();
		for (int i = 0; i < edges.Length; i++){
			edges[i].OnPlayerEntered();
		}
	}

	public void OnPlayerExited () {
		//room.Hide();
		for (int i = 0; i < edges.Length; i++) {
			edges[i].OnPlayerExited();
		}
	}

	public void Show () {
		gameObject.SetActive(true);
	}

	public void Hide (){
		gameObject.SetActive(false);
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            OnPlayerEntered();

            //Debug.Log("entered: " + transform.name);
            //Debug.Log(other.transform);
            //other.GetComponent<Player>.setLocation(this);
        }
            
    }

    void OnTriggerStay(Collider other)
    {
        //Debug.Log("stay trigger: " + transform.name);
        if (other.tag == "Player")
        {

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            OnPlayerExited();
    }
}
