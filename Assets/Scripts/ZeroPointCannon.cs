﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//[RequireComponent(typeof(LineRenderer))]

public class ZeroPointCannon : MonoBehaviour {

    //Vector2 mouse;
    //RaycastHit hit;
    //float range = 100.0f;
    private LineRenderer lr;
    public Transform Barreltr;
    public Material lineMaterial;
    public Image myCrossHairs;

    public int range;
    public int shotLength;
    public int cooldown;
    public int shotCounter;
    public int coolDownCounter;

    public bool canShoot=true;
    public bool cooling = false;
    public bool isShooting=false;
    private float RightTrigger;

    //public Camera playerCamera;
    void Start()
    {
        lr = GetComponentInChildren<LineRenderer>();
        Barreltr = this.transform.GetChild(0);
        //line.SetVertexCount(2);
        lr.GetComponent<Renderer>().material = lineMaterial;
        lr.SetWidth(0.2f, 0.2f);

        //myCrossHairs = GameObject.Find("CrossHairs").GetComponent<Image>();
    }
    void Update()
    {
        if (isShooting)
        {
            shotCounter++;
            if (shotCounter > shotLength)
            {
                shotCounter = 0;
                isShooting = false;
                canShoot = false;
                cooling = true;
                coolDownCounter = 0;
            }

        }
        else
        {
            if (cooling)
            {
                coolDownCounter++;
                if (coolDownCounter > cooldown)
                {
                    coolDownCounter = 0;
                    cooling = false;
                    canShoot = true;
                }
            }

        }
        
        //see if the player is using the gun
        RightTrigger = Input.GetAxisRaw("RightTrigger");
        if (RightTrigger == 1)
        {
            if (!isShooting)
            {
                if (canShoot)
                {
                    isShooting = true;
                    Debug.Log("Trigger pulled, shooting.");
                }
                else
                {
                    //failed shot attempt
                    //isShooting = false;
                    Debug.Log("Trigger pulled, but couldn't shoot.");
                    //play failed shot sound and animation
                }
            }
            else
            {
                coolDownCounter = 0;
                Debug.Log("trigger pulled, currently shooting, reset cooldown");
            }
            
        }
        else
        {
            //not pulling trigger
        }

        //raycast targeting
        RaycastHit hit;
        if (Physics.Raycast(Barreltr.position, Barreltr.forward, out hit))
        {
            if (hit.collider)
            {
                //turn crosshairs green
                myCrossHairs.color = Color.green;
                
                if (isShooting)
                {
                    Shoot(hit);
                }
                else
                {
                    lr.enabled = false;
                }
                //line.enabled = true;
                //line.SetPosition(0, transform.position);
                //line.SetPosition(1, hit.point + hit.normal);
                
            }
            else
            {
                //nothing was hit by the ray
                //lr.SetPosition(1, new Vector3(0, 0, range));
                myCrossHairs.color = Color.white;
            }
            
        }

    }


    void Shoot(RaycastHit hit)
    {
        lr.SetPosition(1, new Vector3(0, 0, hit.distance));
        lr.enabled = true;
        isShooting = true;
        Debug.Log(hit.collider.gameObject);
        //player has hit something
        Instantiate(Resources.Load("ScorchMark"), hit.point, Quaternion.LookRotation(hit.normal));
        //shotCounter++;
        
    }
}
