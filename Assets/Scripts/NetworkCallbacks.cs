﻿using UnityEngine;
using System.Collections;

//server code
[BoltGlobalBehaviour(BoltNetworkModes.Server)]
public class NetworkCallbacks : Bolt.GlobalEventListener
{
    private GameObject tempObj;
    public GameManager gameManagerInstance;
    private int gameSeed;
    private float tempFloat;
    BoltEntity _character;
    private string sceneType;

    //Once the server's scene is finished loading
    public override void SceneLoadLocalDone(string map)
    {
        //the scene loads
        if (GameObject.FindGameObjectWithTag("GameManager") == null)
        {
            //begin a new game in a temple
            Random.seed = generateSeed();
            Debug.Log("Seed: " + Random.seed);

            // instantiate game manager
            //center position
            var pos = new Vector3(0, 0, 0);
            Instantiate(Resources.Load("GameManager"), pos, Quaternion.identity);
            gameManagerInstance = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

            //set seed
//            gameManagerInstance.setGameSeed(gameSeed);
            sceneType = "Temple";
        }
        else
        {
            if (sceneType == "Village")
            {
                var log = LogEvent.Create();
                log.Message = "Entering A New Temple";
                log.Send();

                Debug.Log("New Temple");
                //begin new temple
                sceneType = "Temple";
                gameManagerInstance.spawnNewTemple();
                gameManagerInstance.movePlayersToSpawn();
            }
            else
            {
                var log = LogEvent.Create();
                log.Message = "Entering A New Village";
                log.Send();

                Debug.Log("New Village");
                //begin a new Village
                sceneType = "Village";
                gameManagerInstance.movePlayersToSpawn();
            }
        }
        

    }

    private int generateSeed()
    {
        int Seed;
        float tempFloat;
        tempFloat = Random.value * 100000;
        Seed = (int)tempFloat;
        return Seed;
    }
    public override void SceneLoadRemoteDone(BoltConnection connection)
    {
        gameManagerInstance.newPlayer(connection);
        gameManagerInstance.movePlayersToSpawn();
    }

    public override void Connected(BoltConnection connection)
    {

        var seedInfo = NewGameEvent.Create();
        seedInfo.seed = gameSeed;
        seedInfo.Send();
        Debug.Log("sending seed event");

        //connection.userToken

        var log = LogEvent.Create();
        log.Message = string.Format("{0} connected", connection.RemoteEndPoint);
        log.Send();
    }

    public override void Disconnected(BoltConnection connection)
    {
        var log = LogEvent.Create();
        log.Message = string.Format("{0} disconnected", connection.RemoteEndPoint);
        log.Send();
    }


    public override void ControlOfEntityGained(BoltEntity ply)
    {
        Debug.Log("Server controls " + ply.name);
        ply.transform.Find("Eye").gameObject.SetActive(true);
    }

}
