﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

//runs on all players.
[BoltGlobalBehaviour]
public class PlayerCallbacks : Bolt.GlobalEventListener
{
    List<string> logMessages = new List<string>();
    GUIManager guiManager;

    public override void OnEvent(LogEvent evnt)
    {
        //logMessages.Insert(0, evnt.Message);
        guiManager.logMsg(evnt.Message);
        Debug.Log("EVENT: "+evnt.Message);
    }
    /*
    void OnGUI()
    {
        // only display max the 5 latest log messages
        int maxMessages = Mathf.Min(5, logMessages.Count);

        GUILayout.BeginArea(new Rect(Screen.width / 2 - 200, Screen.height - 100, 400, 100), GUI.skin.box);

        for (int i = 0; i < maxMessages; ++i)
        {
            GUILayout.Label(logMessages[i]);
        }

        GUILayout.EndArea();
    }
     * */

    void Update()
    {
        int maxMessages = Mathf.Min(5, logMessages.Count);
        for (int i = 0; i < maxMessages; ++i)
        {
            //gameLog.GetComponent<Text>().text = logMessages[i];
        }
        
    }

    public override void SceneLoadLocalDone(string map)
    {
        //if there is no gui manager
        if (GameObject.FindGameObjectWithTag("GUIManager") == null)
        {
            //spawn one
            var pos = new Vector3(0, 0, 0);
            Instantiate(Resources.Load("GUIManager"), pos, Quaternion.identity);
            guiManager = GameObject.FindGameObjectWithTag("GUIManager").GetComponent<GUIManager>();
        }
        //turn off overhead cam
        GameObject.FindGameObjectWithTag("MainCamera").SetActive(false);
        
        //gameLog.GetComponent<Text>().text = "stringo";
    }
    
    public override void ControlOfEntityGained(BoltEntity ply)
    {
        //if player object
        if (ply.gameObject.GetComponent<PlayerMotor>() != null)
        {
           
            //Player myPlayer;
            CharacterCreate newCharStats;
            newCharStats = GameObject.FindGameObjectWithTag("NewCharacterData").GetComponent<CharacterCreate>();
            //init character data
            ply.gameObject.GetComponent<Player>().Name = newCharStats.Name;
            ply.gameObject.GetComponent<Player>().maxHealth = newCharStats.Health;
            ply.gameObject.GetComponent<Player>().currentHealth = newCharStats.Health;
            ply.gameObject.GetComponent<Player>().maxStamina = newCharStats.Stamina;
            ply.gameObject.GetComponent<Player>().currentStamina = newCharStats.Stamina;
            ply.gameObject.GetComponent<Player>().speed = newCharStats.Speed;
            ply.gameObject.GetComponent<Player>().dexterity = newCharStats.Dexterity;
            ply.gameObject.GetComponent<Player>().defence = newCharStats.Defence;
            ply.gameObject.GetComponent<Player>().luck = newCharStats.Luck;
            ply.gameObject.GetComponent<Player>().level = 1;
            ply.gameObject.GetComponent<Player>().experience = 0;
            Destroy(GameObject.FindGameObjectWithTag("NewCharacterData"));
        }
           
    }
}