﻿using UnityEngine;
using System.Collections;

public class PlayerMotor : MonoBehaviour
{
    public struct State
    {
        public Vector3 position;
        public Vector3 velocity;
        public bool isGrounded;
        public int jumpFrames;
    }

    public State _state;
    private CharacterController _cc;

    //Speed Variables for the player
    //[HideInInspector]
    public float speed = 10;
    public float defaultSpeed = 10;

    //Gravity force we apply to the player
    public float gravityForce = -9.81f;

    //Jump force that we apply to the player
    public float jumpForce = 12f;

    [SerializeField]
    //Total number of jumpFrames
    private int jumpTotalFrames = 30;

    [SerializeField]
    //This is the drag vector
    private Vector3 drag = new Vector3(1f, 0f, 1f);

    [SerializeField]
    LayerMask layerMask;

    Vector3 sphere
    {
        get
        {
            Vector3 p;

            p = transform.position;
            p.y += _cc.radius;
            //p.y -= (skinWidth * 2);

            return p;
        }
    }

    Vector3 waist
    {
        get
        {
            Vector3 p;

            p = transform.position;
            p.y += _cc.height / 2f;

            return p;
        }
    }

    void Awake() {
        _cc = GetComponent<CharacterController>();
        _state = new State();
        _state.position = transform.localPosition;
        
        //Set the speed to the defaultSpeed
        speed = defaultSpeed;
    }

    public void SetState(Vector3 position, Vector3 velocity, bool isGrounded, int jumpFrames)
    {
        // assign new state
        _state.position = position;
        _state.velocity = velocity;
        _state.jumpFrames = jumpFrames;
        _state.isGrounded = isGrounded;

        // assign local position
        transform.localPosition = _state.position;
    }

    //This method takes care of the player movement, it will also asign the _state.isGrounded value
    void Move(Vector3 velocity)
    {
        bool isGrounded = false;

        isGrounded = isGrounded || _cc.Move(velocity * BoltNetwork.frameDeltaTime) == CollisionFlags.Below;
        isGrounded = isGrounded || _cc.isGrounded;

        if (isGrounded && !_state.isGrounded)
        {
            _state.velocity = new Vector3();
        }

        _state.isGrounded = isGrounded;
        _state.position = transform.localPosition;
    }

    //The is where the method we will update over the network
    public State Movement(float leftRight, float frontBack, bool jump)
    {
        bool isMoving = false;
        Vector3 moveDirection = Vector3.zero;

        //Moving values that we apply when certain buttons are pressed
        //The code below will change the player position X-Z based on the key we pressed
        if (frontBack < 0)
            moveDirection.z += 1;

        if (frontBack > 0)
            moveDirection.z -= 1;

        if (leftRight < 0)
            moveDirection.x -= 1;

        if (leftRight > 0)
            moveDirection.x += 1;

        //If the movement direction is not 0 on X or Z
        //Se set isMoving to true and we apply movement
        if (moveDirection.x != 0 || moveDirection.z != 0)
        {
            if (!_state.isGrounded)
                speed = defaultSpeed / 3f;
            else
                speed = defaultSpeed;

            isMoving = true;
            moveDirection = transform.TransformDirection(moveDirection).normalized;
            //Debug.Log("isMoving "+isMoving);
        }

        //If the player is not grounded we apply falling damage
        if (!_state.isGrounded)
        {
            _state.velocity.y += gravityForce * BoltNetwork.frameDeltaTime;
        }

        //If the player pressed the jump key and jumpFrames is 0 do the code below
        if (jump == true && _state.jumpFrames == 0)
        {
            _state.jumpFrames = (byte)jumpTotalFrames;
            _state.velocity += moveDirection * speed;
        }

        //if the player is moving and the frames are 0 move the player
        Move(moveDirection * speed);

        //If the player jumped and the frames are greater than 0 make the player jump
        if (_state.jumpFrames > 0)
        {
            // calculate force
            float force;
            force = (float)_state.jumpFrames / (float)jumpTotalFrames;
            force = jumpForce * force;

            Move(new Vector3(0, force, 0));
        }

        // decrease jump frames
        _state.jumpFrames = Mathf.Max(0, _state.jumpFrames - 1);

        // apply drag
        _state.velocity.x = ApplyDrag(_state.velocity.x, drag.x);
        _state.velocity.y = ApplyDrag(_state.velocity.y, drag.y);
        _state.velocity.z = ApplyDrag(_state.velocity.z, drag.z);

        // this might seem weird, but it actually gets around a ton of issues - we basically apply 
        // gravity on the Y axis on every frame to simulate instant gravity if you step over a ledge
        _state.velocity.y = Mathf.Min(_state.velocity.y, gravityForce);

        // apply movement
        Move(_state.velocity);

        // update position
        _state.position = transform.localPosition;

        // done
        return _state;
    }

    float ApplyDrag(float value, float drag)
    {
        if (value < 0)
        {
            return Mathf.Min(value + (drag * BoltNetwork.frameDeltaTime), 0f);
        }

        else if (value > 0)
        {
            return Mathf.Max(value - (drag * BoltNetwork.frameDeltaTime), 0f);
        }

        return value;
    }
}