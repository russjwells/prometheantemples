using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour
{
    public bool ShowCursor = false;

    public float sensitivityX = 10F;
    public float sensitivityY = 10F;

    public float minimumY = -90F;
    public float maximumY = 90F;

    public Transform myCamera;

    float rotationX = 0F;
    float rotationY = 0F;

    void Start()
    {
        myCamera = transform.FindChild("Eye");
    }

    void Update() {
        //Hide the cursor
        if (ShowCursor == true)
            Screen.lockCursor = false;
        else
            Screen.lockCursor = true;

        //Show the cursor or hide it when I press F3
        if(Input.GetKeyDown(KeyCode.F3))
        {
            if (ShowCursor == true)
                ShowCursor = false;
            else
                ShowCursor = true;
        }
    }

    public void MouseRotation(float RSXAxis, float RSYAxis)
    {    
        rotationX = RSXAxis * (sensitivityX * Time.deltaTime);
        rotationY = RSYAxis * (sensitivityY * Time.deltaTime);
        rotationY += rotationY * (sensitivityY * BoltNetwork.frameDeltaTime);
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);


        if (transform.tag == "Player")
        {
            transform.Rotate(0,rotationX, 0);
        }

        if (myCamera != null)
        {
            myCamera.transform.Rotate(rotationY, 0, 0);
            //Debug.Log(myCamera.transform.rotation.y);
        }
    }
}